#include <stdio.h>
#include <b.h>

int
main(int argc, char **argv)
{
	printf("This is %s: ", argv[0]);
	return !greatings(argc == 1 ? "world":argv[1]);
}
